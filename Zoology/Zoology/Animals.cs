﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    abstract class Animal
    {
        public bool Fly { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }

        public abstract void Talk();
    }

    class Dog: Animal
    {
        //Dogs can have accessory like clothes
        public DogAccessory Accessory { get; set; }
        public Dog()
        {
            Name = "Bulldog";
            Weight = 25;
            Accessory = new DogAccessory("bandana", "blue");
        }
        public Dog(string name, int weight, string clothes, string color)
        {
            Name = name;
            Weight = weight;
            Accessory = new DogAccessory(clothes, color);
        }
        public void Run()
        {
            Console.WriteLine($"{Name} is running very fast");
        }

        public override void Talk()
        {
            Console.WriteLine($"{Name}: \"Bark Bark\""); ;
        }
    }

    class Bird: Animal
    {
        public Bird(string name, int weight, bool fly)
        {
            Name = name;
            Weight = weight;
            Fly = fly;
        }

        public override void Talk()
        {
            Console.WriteLine($"{Name}: \"ChirpChirp\""); ;
        }

        public void FlyHigh()
        {
            if (Fly)
            {
                Console.WriteLine($"{Name} flies away");
            }
            else
            {
                Console.WriteLine($"{Name} flutter with wings, but does not work! Must run");
            }
        }
    }

    class DogAccessory
    {
        public string Clothes { get; set; }
        public string Color { get; set; }

        public DogAccessory(string clothes, string color)
        {
            Clothes = clothes;
            Color = color;
        }
    }
}
