﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initialize a list with animals for the zoo
            List<Animal> animals = new List<Animal>() { };
            InitializeAnimals(animals);

            PrintInfoAboutZooAnimals(animals);
        }

        static void PrintInfoAboutZooAnimals(List<Animal> animals)
        {
            Console.WriteLine("Now we are going to see what animals are in the zoo");
            foreach (Animal animal in animals)
            {
                Console.WriteLine($"{animal.GetType().Name}, Type: {animal.Name}, weight: " +
                    $"{animal.Weight}");
                //All animals makes a sound
                animal.Talk();

                //Dogs and Birds have different functionality
                if (animal.GetType().Name == "Dog")
                {
                    Dog dog = (Dog)animal;
                    Console.WriteLine($"This {dog.Name} is wearing a {dog.Accessory.Color} {dog.Accessory.Clothes}");
                    dog.Run();
                }
                else if (animal.GetType().Name == "Bird")
                {
                    Bird bird = (Bird)animal;
                    bird.FlyHigh();
                }
                Console.WriteLine();
            }
        }

        static void InitializeAnimals(List<Animal> animals)
        {
            animals.Add(new Dog("Pug", 9, "coat", "grey"));
            animals.Add(new Dog());
            animals.Add(new Bird("Ostrich", 120, false));
            animals.Add(new Bird("Parrot", 4, true));
        }
        
    }
}
